# Pay Day Bank Application


This application has been written as a distributed systems.
There are six microservices
- ms-gw
- ms-user
- ms-account
- ms-order
- ms-stock
- ms-notification

<a href="https://ibb.co/DGF49J3"><img src="https://i.ibb.co/q5k32wy/pay-daya-acrhitecture.png"
 alt="pay-day-bank-2021-02-08-00-33-49" border="0"></a>
<br/><br/>
I have used Consul as a service registery&discovery.<br/>
By using the below link we can see registered instance of microservices.<br/>
[http://localhost:8500](http://localhost:8500 "http://localhost:8500")<br/>
</br><br/><br/>

To run application firstly should add **EMAIL** and **EMAIL_PASSWORD** then follow the below command:<br/>
`docker-compose up`

</br><br/>
I have implemented sell and buy operations like Choreography-based saga by using <br/>
Kafka(every operations triggers the next operation) but haven't implemented compensation methods<br/> 
to reject order in any error case.<br/>
Bcoz in that case will need some retry mechanism which will take more time to implement.<br/>
If order successfully completed in ab external system then <br/>
we have to complete in our side by using retry or any other operations. <br/>
<br/>
This application can be improved a lot, bcoz of limited time just
I have done what can I do in three days.
It can be improved in the below components.
-  Authentication
-  Database(Postgres, MySQL etc)
-  Logging
-  Container orchestration(Minikube or Vagrant)

<br/><br/><br/>
I paste the simple roadmap to test how this application works.<br/><br/>
#### Create User
[http://localhost:8081/api/user/users](http://localhost:8081/api/user/users "http://localhost:8081/api/user/users")
```json
{
	"name":"Mehman",
	"surname":"Bashirov",
	"email":"mehmanbashirov@gmail.com",
	"username":"bashirovma",
	"password":"mehman123"
}
```
Response:(Just return return created user with id, but we can take from somewhere like from SecurityContext)
```json
{
    "data": {
        "id": 1,
        "name": "Mehman",
        "surname": "Bashirov",
        "email": "mehmanbashirov@gmail.com",
        "username": "bashirovma"
    }
}
```
<br/><br/>
#### Create Account
[http://localhost:8081/api/account/accounts](http://localhost:8081/api/account/accounts "http://localhost:8081/api/account/accounts")
POST Request:
```json
{
	"userId":1
}
```
Response:
```json
{
    "id": 1,
    "userId": 1,
    "accountNumber": "3881049878177208181",
    "balance": null,
    "creationDate": "2021-02-08T03:53:15.957688"
}
```

<br/><br/>
#### Deposit Balance
[http://localhost:8081/api/account/accounts/3881049878177208181/deposit](http://localhost:8081/api/account/accounts/3881049878177208181/deposit "http://localhost:8081/api/account/accounts/3881049878177208181/deposit")
PUT Request:
```json
{
    "depositAmount": 5000
}
```
Response: HttpStatus

<br/><br/>
#### Get account cash balance
[http://localhost:8081/api/account/accounts/3881049878177208181/cash-balance](http://localhost:8081/api/account/accounts/3881049878177208181/cash-balance "http://localhost:8081/api/account/accounts/3881049878177208181/cash-balance")
GET Request
Response:
```json
{
    "data": {
        "balance": 5000.00
    }
}
```
<br/><br/>

#### Create Order
[http://localhost:8081/api/order/orders](http://localhost:8081/api/order/orders "http://localhost:8081/api/order/orders")
Post Request:
```json
`{
    "userId":1,
    "stockId":1,
    "accountNumber":"3881049878177208181",
    "quantity":1,
    "targetPrice":10,
    "orderType":"BUY"
}`
```
Response: HttpStatus

<br/><br/>

#### Get account total balance
[http://localhost:8081/account/accounts/3881045949832658225/total-balance](http://localhost:8081/account/accounts/3881045949832658225/total-balance "http://localhost:8081/account/accounts/3881045949832658225/total-balance")
<br/>This endpoint can not be completed.<br/>
This consist of two parts.<br/>
Cash balance + real time stock balance(By taking from redis cache)
