package az.ibar.order.controller;

import az.ibar.order.model.OrderType;
import az.ibar.order.model.dto.OrderDto;
import az.ibar.order.service.OrderService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static az.ibar.order.config.Serializer.convertToJson;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("User controller unit tests")
@ExtendWith(SpringExtension.class)
@WebMvcTest(OrderController.class)
class OrderControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderService orderService;

    @Test
    void createOrder() throws Exception {

        var orderDto = OrderDto.builder()
                .stockId(1L)
                .targetPrice(new BigDecimal("1750"))
                .orderType(OrderType.BUY)
                .quantity(2)
                .accountNumber("3801")
                .build();

        mvc.perform(post("/orders")
                .contentType(APPLICATION_JSON)
                .content(convertToJson(orderDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.data").value(equalTo("Operation successfully completed")));

        verify(orderService, times(1)).createOrder(any());
    }
}