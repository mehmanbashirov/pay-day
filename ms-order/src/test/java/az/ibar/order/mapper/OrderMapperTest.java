package az.ibar.order.mapper;

import az.ibar.order.model.OrderState;
import az.ibar.order.model.OrderType;
import az.ibar.order.model.dto.OrderDto;
import az.ibar.order.model.entity.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Order mapper tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {OrderMapperImpl.class})
class OrderMapperTest {

    @Autowired
    private OrderMapper orderMapper;

    private Order expected;
    private OrderDto orderDto;


    @BeforeEach
    void setUp() {
        orderDto = OrderDto.builder()
                .orderType(OrderType.BUY)
                .accountNumber("3801")
                .quantity(2)
                .targetPrice(new BigDecimal("1750"))
                .stockId(1L)
                .build();

        expected = Order.builder()
                .orderType(OrderType.BUY)
                .accountNumber("3801")
                .quantity(2)
                .orderState(OrderState.PENDING)
                .targetPrice(new BigDecimal(1750))
                .stockId(1L)
                .build();
    }


    @Test
    @DisplayName("Given OrderDto when toOrder then Order")
    void toOrder() {
        Order actual = orderMapper.toOrder(orderDto);

        assertThat(actual).isEqualToComparingFieldByField(expected);
    }
}