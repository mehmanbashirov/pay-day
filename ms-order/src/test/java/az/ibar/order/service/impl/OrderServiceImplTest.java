package az.ibar.order.service.impl;

import az.ibar.order.client.account.model.AccountStock;
import az.ibar.order.error.InsufficientBalanceException;
import az.ibar.order.error.OrderNotFoundException;
import az.ibar.order.mapper.OrderMapper;
import az.ibar.order.messages.NotificationMessageSender;
import az.ibar.order.messages.OrderMessageSender;
import az.ibar.order.messages.model.CompleteOrderConsumer;
import az.ibar.order.messages.model.OrderNotificationMessage;
import az.ibar.order.model.OrderType;
import az.ibar.order.model.dto.OrderDto;
import az.ibar.order.model.entity.Order;
import az.ibar.order.repository.OrderRepository;
import az.ibar.order.service.AccountService;
import az.ibar.order.service.OrderService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("Order service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {OrderServiceImpl.class})
class OrderServiceImplTest {

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private OrderMessageSender<OrderDto> orderMessageSender;

    @MockBean
    private AccountService accountService;

    @MockBean
    private OrderMapper orderMapper;

    @MockBean
    private NotificationMessageSender<OrderNotificationMessage> notificationSender;


    @Autowired
    private OrderService orderService;

    private OrderDto orderDto;
    private Order orderEntity;
    private final String ACCOUNT_NUMBER = "38810944000000000000";
    private final Integer QUANTITY = 1;


    @Test
    void createBuyOrderErrorCase() {
        orderDto = OrderDto.builder()
                .orderType(OrderType.BUY)
                .accountNumber(ACCOUNT_NUMBER)
                .quantity(QUANTITY)
                .targetPrice(new BigDecimal("1750"))
                .stockId(1L)
                .build();

        when(accountService.getAccountBalance(any())).thenReturn(new BigDecimal("1749"));

        assertThatExceptionOfType(InsufficientBalanceException.class)
                .isThrownBy(() -> orderService.createOrder(orderDto))
                .withMessage("You don not have enough account cash balance to make such an order");

        verify(accountService, times(1)).getAccountBalance(any());
    }


    @Test
    @DisplayName("Given OrderDto when createOrder then create order")
    void createBuyOrder() {
        orderDto = OrderDto.builder()
                .orderType(OrderType.BUY)
                .accountNumber(ACCOUNT_NUMBER)
                .quantity(QUANTITY)
                .targetPrice(new BigDecimal("1750"))
                .stockId(1L)
                .build();

        orderEntity = Order.builder()
                .id(1L)
                .orderType(OrderType.BUY)
                .accountNumber(ACCOUNT_NUMBER)
                .quantity(QUANTITY)
                .targetPrice(new BigDecimal(1750))
                .stockId(1L)
                .build();

        orderDto = OrderDto.builder()
                .orderType(OrderType.BUY)
                .accountNumber(ACCOUNT_NUMBER)
                .quantity(QUANTITY)
                .targetPrice(new BigDecimal("1750"))
                .stockId(1L)
                .build();

        when(accountService.getAccountBalance(any())).thenReturn(new BigDecimal("1755"));
        when(orderMapper.toOrder(any())).thenReturn(orderEntity);
        when(orderRepository.save(any())).thenReturn(orderEntity);

        orderService.createOrder(orderDto);

        verify(accountService, times(1)).getAccountBalance(any());
        verify(accountService, times(1)).holdAccountAmount(orderDto);
        verify(orderMessageSender, times(1)).send(any());
    }


    @Test
    @DisplayName("Given OrderDto when createOrder then throw InsufficientBalanceException")
    void createSellOrderInErrorCase() {
        orderDto = OrderDto.builder()
                .orderType(OrderType.SELL)
                .accountNumber(ACCOUNT_NUMBER)
                .quantity(QUANTITY)
                .targetPrice(new BigDecimal("1750"))
                .stockId(1L)
                .build();
        when(accountService.getAccountStocks(any())).thenReturn(List.of(AccountStock.builder().stockId(1L).quantity(0).build()));

        assertThatExceptionOfType(InsufficientBalanceException.class)
                .isThrownBy(() -> orderService.createOrder(orderDto))
                .withMessage("You do not have enough stock in your account balance");

        verify(accountService, times(1)).getAccountStocks(any());
    }


    @Test
    @DisplayName("Given OrderDto when createOrder then should create sell order")
    void createSellOrderInSuccessCase() {
        orderDto = OrderDto.builder()
                .orderType(OrderType.SELL)
                .accountNumber(ACCOUNT_NUMBER)
                .quantity(QUANTITY)
                .targetPrice(new BigDecimal("1750"))
                .stockId(1L)
                .build();

        orderEntity = Order.builder()
                .id(1L)
                .orderType(OrderType.BUY)
                .accountNumber(ACCOUNT_NUMBER)
                .quantity(QUANTITY)
                .targetPrice(new BigDecimal(1750))
                .stockId(1L)
                .build();

        when(accountService.getAccountStocks(any())).thenReturn(List.of(AccountStock.builder().stockId(1L).quantity(5).build()));
        when(orderMapper.toOrder(any())).thenReturn(orderEntity);
        when(orderRepository.save(any())).thenReturn(orderEntity);

        orderService.createOrder(orderDto);

        verify(accountService, times(1)).holdAccountAmount(orderDto);
        verify(orderMessageSender, times(1)).send(any());
    }


    @Test
    @DisplayName("Given non existence orderId when completeOrder then should thorw OrderNotFoundException")
    void completeOrder() {
        var completeOrderConsumer = CompleteOrderConsumer.builder()
                .orderId(1L)
                .build();

        when(orderRepository.findById(any())).thenReturn(Optional.empty());

        assertThatExceptionOfType(OrderNotFoundException.class)
                .isThrownBy(() -> orderService.completeOrder(completeOrderConsumer))
                .withMessage("Order not found");

        verify(orderRepository, times(1)).findById(any());
    }


    @Test
    @DisplayName("Given orderId when completeOrder then should complete order")
    void completeOrderInSuccessCase() {
        var completeOrderConsumer = CompleteOrderConsumer.builder()
                .orderId(1L)
                .orderType(OrderType.BUY)
                .build();
        when(orderRepository.findById(any())).thenReturn(Optional.of(Order.builder().id(1L).build()));

        orderService.completeOrder(completeOrderConsumer);

        verify(orderRepository, times(1)).findById(any());
        verify(orderRepository, times(1)).save(any());
        verify(notificationSender, times(1)).send(any());
    }
}