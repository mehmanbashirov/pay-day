package az.ibar.order.service.impl;

import az.ibar.order.client.account.MsAccountClient;
import az.ibar.order.client.account.model.AccountBalanceResponseDto;
import az.ibar.order.client.account.model.AccountStock;
import az.ibar.order.error.model.RestResponse;
import az.ibar.order.model.OrderType;
import az.ibar.order.model.dto.OrderDto;
import az.ibar.order.service.AccountService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("Account service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AccountServiceImpl.class})
class AccountServiceImplTest {

    @MockBean
    private MsAccountClient msAccountClient;

    @Autowired
    private AccountService accountService;


    @Test
    @DisplayName("Given orderDto when holdAccountAmount then should return nothing")
    void holdAccountAmount() {
        var orderDto = OrderDto.builder()
                .orderType(OrderType.BUY)
                .accountNumber("3801")
                .quantity(2)
                .targetPrice(new BigDecimal(1744))
                .stockId(1L)
                .build();

        accountService.holdAccountAmount(orderDto);
        verify(msAccountClient, times(1)).holdAmount(any(), any());
    }


    @Test
    @DisplayName("Given accountNumber when getAccountBalance then should return account cash balance")
    void getAccountBalance() {
        RestResponse<AccountBalanceResponseDto> cashBalanceResponse = new RestResponse<>(AccountBalanceResponseDto.of(BigDecimal.ONE));
        when(msAccountClient.getAccountCashBalance(any())).thenReturn(cashBalanceResponse);

        BigDecimal accountCashBalance = accountService.getAccountBalance(any());

        assertEquals(cashBalanceResponse.getData().getBalance(), accountCashBalance);

        verify(msAccountClient, times(1)).getAccountCashBalance(any());
    }


    @Test
    @DisplayName("Given accountNumber when getAccountStocks then should return account stock list")
    void getAccountStocks() {
        var accountStock = AccountStock
                .builder()
                .orderPrice(new BigDecimal("1750"))
                .stockId(1L)
                .orderType(OrderType.BUY)
                .quantity(2)
                .build();


        when(msAccountClient.getAccountStocks(any())).thenReturn(List.of(accountStock));

        List<AccountStock> actual = accountService.getAccountStocks(any());

        verify(msAccountClient, times(1)).getAccountStocks(any());

        assertThat(actual).isEqualTo(List.of(accountStock));
    }
}