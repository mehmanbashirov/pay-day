package az.ibar.order.error;

public class OrderNotFoundException extends CommonException {

    public OrderNotFoundException() {
        super("DATA_NOT_FOUND", "Order not found");
    }
}
