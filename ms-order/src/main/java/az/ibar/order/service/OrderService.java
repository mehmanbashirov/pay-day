package az.ibar.order.service;

import az.ibar.order.messages.model.CompleteOrderConsumer;
import az.ibar.order.model.dto.OrderDto;

public interface OrderService {

    void createOrder(OrderDto orderDto);

    void completeOrder(CompleteOrderConsumer consumer);

}
