package az.ibar.order.model;

public enum OrderState {

    PENDING, COMPLETED

}
