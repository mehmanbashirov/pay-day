package az.ibar.notification.service.impl;

import az.ibar.notification.client.MsUserClient;
import az.ibar.notification.client.model.UserDto;
import az.ibar.notification.error.model.RestResponse;
import az.ibar.notification.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("User service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UserServiceImpl.class})
class UserServiceImplTest {

    @MockBean
    private MsUserClient msUserClient;

    @Autowired
    private UserService userService;

    @Test
    void findById() {

        var userDto = UserDto.builder()
                .surname("Testov")
                .email("test@test.com")
                .build();

        when(msUserClient.findById(any())).thenReturn(new RestResponse<>(userDto));

        UserDto actual = userService.findById(any());

        assertThat(actual).isEqualToComparingFieldByField(userDto);
    }
}