package az.ibar.notification.service.impl;

import az.ibar.notification.service.MailService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@DisplayName("Mail service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {MailServiceImpl.class})
class MailServiceImplTest {

    @MockBean
    private JavaMailSender javaMailSender;

    @Autowired
    private MailService mailService;

    @Test
    void sendMail() {
        mailService.sendMail("", "", "");

        verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
    }
}