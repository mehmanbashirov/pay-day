package az.ibar.notification.messages;

import az.ibar.notification.client.model.UserDto;
import az.ibar.notification.messages.model.MailConsumer;
import az.ibar.notification.messages.model.Message;
import az.ibar.notification.service.MailService;
import az.ibar.notification.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static az.ibar.notification.config.Serializer.convertToJson;
import static az.ibar.notification.util.DefaultValueUtil.ORDER_COMPLETION_SUBJECT;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("Notification MessageListener tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {MessageListener.class})
class MessageListenerTest {


    @MockBean
    private UserService userService;

    @MockBean
    private MailService mailService;

    @MockBean
    private ObjectMapper objectMapper;

    @Autowired
    private MessageListener messageListener;


    @Test
    void handle() throws JsonProcessingException {

        var mailConsumer = MailConsumer.of(1L, "Order completion message");
        Message<MailConsumer> completeOrderMessage = new Message<>(mailConsumer);

        when(objectMapper.readValue(anyString(), any(TypeReference.class)))
                .thenReturn(completeOrderMessage);

        var userDto = UserDto.builder()
                .email("email@gmail.com")
                .name("name")
                .surname("surname")
                .build();

        when(userService.findById(any())).thenReturn(userDto);

        messageListener.handle(convertToJson(completeOrderMessage));

        verify(userService, times(1)).findById(any());
        verify(mailService, times(1))
                .sendMail(userDto.getEmail(), ORDER_COMPLETION_SUBJECT, mailConsumer.getMessage());
    }


    @Test
    void handleMessageListenerErrorCase() {
        assertThatExceptionOfType(NullPointerException.class)
                .isThrownBy(() -> messageListener.handle(convertToJson("test")));
    }
}