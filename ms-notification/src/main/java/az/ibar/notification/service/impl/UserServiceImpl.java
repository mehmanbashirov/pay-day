package az.ibar.notification.service.impl;

import az.ibar.notification.client.MsUserClient;
import az.ibar.notification.client.model.UserDto;
import az.ibar.notification.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final MsUserClient msUserClient;

    @Override
    public UserDto findById(Long userId) {
        return msUserClient.findById(userId).getData();
    }
}
