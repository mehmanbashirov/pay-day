package az.ibar.account.service.impl;

import az.ibar.account.error.AccountNotFoundException;
import az.ibar.account.error.InsufficientBalanceException;
import az.ibar.account.mapper.AccountMapper;
import az.ibar.account.messages.CompleteOrderMessageSender;
import az.ibar.account.messages.model.AccountStockConsumer;
import az.ibar.account.messages.model.CompleteOrderProducer;
import az.ibar.account.model.dto.AccountDepositDto;
import az.ibar.account.model.dto.AccountHoldDto;
import az.ibar.account.model.dto.AccountRequestDto;
import az.ibar.account.model.dto.AccountResponseDto;
import az.ibar.account.model.dto.OrderType;
import az.ibar.account.model.entity.Account;
import az.ibar.account.model.entity.AccountStock;
import az.ibar.account.repository.AccountRepository;
import az.ibar.account.service.AccountService;
import az.ibar.account.service.StockService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("Account service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AccountServiceImpl.class})
class AccountServiceImplTest {

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private AccountMapper accountMapper;

    @MockBean
    private CompleteOrderMessageSender<CompleteOrderProducer> completeOrderMessageSender;

    @MockBean
    private StockService stockService;

    @Autowired
    private AccountService accountService;

    private Account account;

    private final String ACCOUNT_NUMBER = "3881049878177208181";
    private final BigDecimal ACCOUNT_BALANCE = BigDecimal.valueOf(5000);


    @Test
    @DisplayName("Given AccountRequestDto when saveAccount then should return AccountResponseDto")
    void save() {
        Long userId = 1L;
        var accountRequestDto = AccountRequestDto.builder().userId(userId).build();

        account = Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .balance(ACCOUNT_BALANCE)
                .userId(userId)
                .build();

        var accountResponseDto = AccountResponseDto.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .balance(ACCOUNT_BALANCE)
                .build();

        when(accountRepository.save(any())).thenReturn(account);
        when(accountMapper.toAccountResponseDto(account)).thenReturn(accountResponseDto);

        AccountResponseDto actual = accountService.save(accountRequestDto);

        assertThat(actual).isEqualToComparingFieldByField(accountResponseDto);

        verify(accountRepository, times(1)).save(any());
    }


    @Test
    @DisplayName("Given accountNumber with not enough balance when holdAmount then throw InsufficientBalanceException")
    void holdAmountErrorCase() {

        var account = Account.builder()
                .balance(ACCOUNT_BALANCE)
                .accountNumber(ACCOUNT_NUMBER)
                .build();

        var accountHoldDto = AccountHoldDto.builder()
                .holdAmount(BigDecimal.valueOf(6000))
                .build();

        when(accountRepository.findByAccountNumber(any()))
                .thenReturn(Optional.of(account));

        assertThatExceptionOfType(InsufficientBalanceException.class)
                .isThrownBy(() -> accountService.holdAmount(ACCOUNT_NUMBER, accountHoldDto))
                .withMessage("There is not enough balance in your account");

        verify(accountRepository, times(1)).findByAccountNumber(any());
    }


    @Test
    @DisplayName("Given accountNumber and AccountHoldDto when holdAmount then hold-amount")
    void holdAmountSuccessCase() {
        account = Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .balance(ACCOUNT_BALANCE)
                .id(1L)
                .holdAmount(new BigDecimal("865"))
                .build();

        var accountHoldDto = AccountHoldDto.builder()
                .holdAmount(new BigDecimal("1000"))
                .build();

        when(accountRepository.findByAccountNumber(any()))
                .thenReturn(Optional.of(account));

        accountService.holdAmount(any(), accountHoldDto);

        verify(accountRepository, times(1)).findByAccountNumber(any());
    }


    @Test
    @DisplayName("Given non existence accountNumber when getAccountCashBalance then throw AccountNotFoundException")
    void getAccountCashBalanceErrorCase() {
        when(accountRepository.findByAccountNumber(any())).thenReturn(Optional.empty());

        assertThatExceptionOfType(AccountNotFoundException.class)
                .isThrownBy(() -> accountService.getAccountCashBalance(any()))
                .withMessage("Account not found");

        verify(accountRepository, times(1)).findByAccountNumber(any());
    }


    @Test
    @DisplayName("Given accountNumber when getAccountCashBalance then should return AccountBalanceResponseDto")
    void getAccountCashBalanceSuccessCase() {
        var account = Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .balance(ACCOUNT_BALANCE)
                .holdAmount(new BigDecimal("1000"))
                .build();

        when(accountRepository.findByAccountNumber(any())).thenReturn(Optional.of(account));

        var accountBalanceResponseDto = accountService.getAccountCashBalance(any());

        assertEquals(account.getBalance().subtract(account.getHoldAmount()), accountBalanceResponseDto.getBalance());

        verify(accountRepository, times(1)).findByAccountNumber(any());
    }


    @Test
    @DisplayName("Given AccountStockDto when update then update")
    void getAccountStocks() {
        Long stockId = 1L;
        Integer quantity = 1;

        var accountStock = AccountStock.builder()
                .stockId(stockId)
                .quantity(quantity)
                .build();

        account = Account.builder()
                .accountStocks(List.of(accountStock))
                .build();

        var accountStockConsumer = AccountStockConsumer.builder()
                .orderType(OrderType.BUY)
                .accountNumber(ACCOUNT_NUMBER)
                .stockId(stockId)
                .quantity(quantity)
                .orderPrice(BigDecimal.valueOf(500))
                .build();

        var completeOrderProducer = CompleteOrderProducer.builder()
                .orderType(OrderType.BUY)
                .accountNumber(ACCOUNT_NUMBER)
                .stockId(stockId)
                .quantity(quantity)
                .orderPrice(BigDecimal.valueOf(500))
                .build();

        when(accountRepository.findByAccountNumber(ACCOUNT_NUMBER)).thenReturn(Optional.of(account));

        when(accountMapper.toCompleteOrderProducer(any())).thenReturn(completeOrderProducer);

        accountService.update(accountStockConsumer);

        verify(completeOrderMessageSender, times(1)).send(any());

    }


    @Test
    @DisplayName("Given accountNumber when deposit then throw AccountNotFoundException")
    void depositAccountErrorCase() {
        when(accountRepository.findByAccountNumber(any())).thenReturn(Optional.empty());

        assertThatExceptionOfType(AccountNotFoundException.class)
                .isThrownBy(() -> accountService.depositAccount(ACCOUNT_NUMBER, null))
                .withMessage("Account not found");

        verify(accountRepository, times(1)).findByAccountNumber(any());
    }


    @Test
    @DisplayName("Given accountNumber and AccountDepositDto when deposit then deposit")
    void depositAccountSuccessCase() {
        account = Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .balance(ACCOUNT_BALANCE)
                .build();

        var accountDepositDto = AccountDepositDto.builder()
                .depositAmount(BigDecimal.valueOf(5000))
                .build();

        when(accountRepository.findByAccountNumber(any())).thenReturn(Optional.of(account));

        accountService.depositAccount(ACCOUNT_NUMBER, accountDepositDto);

        verify(accountRepository, times(1)).findByAccountNumber(any());
    }


}