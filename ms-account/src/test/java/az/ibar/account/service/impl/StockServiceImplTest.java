package az.ibar.account.service.impl;

import az.ibar.account.model.entity.Stock;
import az.ibar.account.repository.StockRepository;
import az.ibar.account.service.StockService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@DisplayName("Stock service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {StockServiceImpl.class})
class StockServiceImplTest {

    @MockBean
    private StockRepository stockRepository;

    @Autowired
    private StockService stockService;


    @Test
    @DisplayName("Given nothing when getStocks then should return List of Stock")
    void getStocks() {
        var stock = Stock.builder()
                .id(1L)
                .name("TESLA")
                .price(new BigDecimal("859"))
                .build();

        List<Stock> expected = List.of(stock);

        when(stockRepository.getStocks()).thenReturn(List.of(stock));

        List<Stock> actual = stockService.getStocks();

        assertThat(actual).isEqualTo(expected);
    }
}