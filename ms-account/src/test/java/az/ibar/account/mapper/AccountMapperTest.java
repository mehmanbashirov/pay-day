package az.ibar.account.mapper;

import az.ibar.account.messages.model.AccountStockConsumer;
import az.ibar.account.model.dto.AccountResponseDto;
import az.ibar.account.model.dto.AccountStockDto;
import az.ibar.account.model.dto.OrderType;
import az.ibar.account.model.entity.Account;
import az.ibar.account.model.entity.AccountStock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Account mapper tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AccountMapperImpl.class})
class AccountMapperTest {

    @Autowired
    private AccountMapper accountMapper;

    private final String ACCOUNT_NUMBER = "3881049878177208181";
    private final BigDecimal ACCOUNT_BALANCE = BigDecimal.valueOf(5000);
    private final Long STOCK_ID = 1L;
    private final Integer QUANTITY = 1;


    @Test
    @DisplayName("Given Account entity when toAccountResponseDto then should return AccountResponseDto")
    void toAccountResponseDto() {
        var account = Account.builder()
                .balance(ACCOUNT_BALANCE)
                .accountNumber(ACCOUNT_NUMBER)
                .build();

        var accountResponseDto = AccountResponseDto.builder()
                .balance(ACCOUNT_BALANCE)
                .accountNumber(ACCOUNT_NUMBER)
                .build();


        var actual = accountMapper.toAccountResponseDto(account);

        assertThat(actual).isEqualToComparingFieldByField(accountResponseDto);
    }


    @Test
    @DisplayName("Given AccountStockDto when toAccountStock then should return AccountStock")
    void toAccountStock() {
        var accountStockConsumer = AccountStockConsumer.builder()
                .orderType(OrderType.BUY)
                .stockId(STOCK_ID)
                .quantity(QUANTITY)
                .orderId(1L)
                .orderPrice(BigDecimal.valueOf(1720))
                .accountNumber(ACCOUNT_NUMBER)
                .build();

        AccountStock actual = accountMapper.toAccountStock(accountStockConsumer);

        assertEquals(actual.getAccount().getAccountNumber(), accountStockConsumer.getAccountNumber());
        assertEquals(actual.getStockId(), accountStockConsumer.getStockId());
        assertEquals(actual.getQuantity(), accountStockConsumer.getQuantity());
    }

    @Test
    @DisplayName("Given AccountStock when toAccountStockDto then should return AccountStockDto")
    void toAccountStockDto() {
        var account = Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .build();

        var accountStock = AccountStock.builder()
                .stockId(STOCK_ID)
                .quantity(QUANTITY)
                .account(account)
                .build();

        var accountStockDto = AccountStockDto.builder()
                .stockId(STOCK_ID)
                .quantity(QUANTITY)
                .accountNumber(ACCOUNT_NUMBER)
                .build();

        var actual = accountMapper.toAccountStockDto(accountStock);

        assertThat(actual).isEqualToComparingFieldByField(accountStockDto);
    }


    @Test
    @DisplayName("Given List of AccountStock when toAccountStockDtoList then should return List of AccountStockDto")
    void toAccountStockDtoList() {
        var account = Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .build();

        var accountStock = AccountStock.builder()
                .stockId(STOCK_ID)
                .quantity(QUANTITY)
                .account(account)
                .build();

        var accountStockDto = AccountStockDto.builder()
                .stockId(STOCK_ID)
                .quantity(QUANTITY)
                .accountNumber(ACCOUNT_NUMBER)
                .build();

        List<AccountStockDto> actual = accountMapper.toAccountStockDtoList(List.of(accountStock));

        assertThat(actual).isEqualTo(List.of(accountStockDto));
    }
}