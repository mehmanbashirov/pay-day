package az.ibar.account.repository.impl;

import az.ibar.account.model.entity.Stock;
import az.ibar.account.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Repository;

import java.util.List;

import static az.ibar.account.util.DefaultValueUtil.STOCK_BUCKET_NAME;

@Repository
@RequiredArgsConstructor
public class StockRepositoryImpl implements StockRepository {

    private final RedissonClient redissonClient;

    @Override
    public List<Stock> getStocks() {
        return redissonClient.getList(STOCK_BUCKET_NAME);
    }

}
