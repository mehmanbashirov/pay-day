package az.ibar.user.mapper;

import az.ibar.user.model.dto.UserDto;
import az.ibar.user.model.dto.UserResponseDto;
import az.ibar.user.model.entity.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("User mapper tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UserMapperImpl.class})
class UserMapperTest {

    @Autowired
    private UserMapper userMapper;

    private static User userEntity;


    @BeforeAll
    public static void setUp() {
        userEntity = User.builder()
                .name("Test")
                .email("test@test.com")
                .build();
    }

    @Test
    @DisplayName("Given UserDto when map to user entity then should return User")
    void givenUserDto_whenToUser_thenReturnUserEntity() {
        var userDto = UserDto.builder()
                .name("Test")
                .email("test@test.com")
                .build();

        User actual = userMapper.toUserEntity(userDto);

        assertThat(actual).isEqualToComparingFieldByField(userEntity);
    }


    @Test
    @DisplayName("Given User entity when map to UserDto then should return UserDto")
    void givenUserEntity_whenToUserDto_thenReturnUserDto() {
        var userDto = UserDto.builder()
                .name("Test")
                .email("test@test.com")
                .build();

        UserDto actual = userMapper.toUserDto(userEntity);

        assertThat(actual).isEqualToComparingFieldByField(userDto);
    }


    @Test
    @DisplayName("Given User entity when map to UserResponseDto then should return UserResponseDto")
    void givenUserEntity_whenToUserDto_thenReturnUserResponseDto() {
        var userResponseDto = UserResponseDto.builder()
                .name("Test")
                .email("test@test.com")
                .build();

        UserResponseDto actual = userMapper.toUserResponseDto(userEntity);

        assertThat(actual).isEqualToComparingFieldByField(userResponseDto);
    }
}