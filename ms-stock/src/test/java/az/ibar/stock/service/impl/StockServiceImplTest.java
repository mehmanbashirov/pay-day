package az.ibar.stock.service.impl;

import az.ibar.stock.model.Stock;
import az.ibar.stock.repository.StockRepository;
import az.ibar.stock.service.StockService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("Stock service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {StockServiceImpl.class})
class StockServiceImplTest {

    @MockBean
    private StockRepository stockRepository;

    @Autowired
    private StockService stockService;


    @Test
    @DisplayName("Given nothing when updateStockPrice then get real-time prices, save to cache and return nothing")
    void updateStockPrice() {
        doNothing().when(stockRepository).saveStock(any());

        stockService.updateStockPrice();

        verify(stockRepository, times(1)).saveStock(any());
    }


    @Test
    @DisplayName("Given nothing when get stock list then should return list of stocks from cache")
    void getStocks() {
        List<Stock> expected = List.of(Stock.builder().name("TESLA").price(BigDecimal.ONE).build());

        when(stockRepository.getStocks()).thenReturn(expected);

        List<Stock> actual = stockService.getStocks();

        Assertions.assertThat(actual).isEqualTo(expected);

    }

}