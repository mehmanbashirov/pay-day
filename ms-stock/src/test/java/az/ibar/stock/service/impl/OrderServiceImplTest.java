package az.ibar.stock.service.impl;

import az.ibar.stock.messages.CompleteStockOrderMessageSender;
import az.ibar.stock.messages.model.AccountStockProducer;
import az.ibar.stock.model.OrderDto;
import az.ibar.stock.model.OrderType;
import az.ibar.stock.model.Stock;
import az.ibar.stock.repository.OrderRepository;
import az.ibar.stock.service.OrderService;
import az.ibar.stock.service.StockService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("Order service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {OrderServiceImpl.class})
class OrderServiceImplTest {

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private StockService stockService;

    @MockBean
    private CompleteStockOrderMessageSender<Long> completeOrderMessageSender;

    @MockBean
    private CompleteStockOrderMessageSender<AccountStockProducer> accountCompleteStockOrderMessageSender;

    @Autowired
    private OrderService orderService;

    private List<OrderDto> orderDtoList;
    private List<Stock> stockList;

    @BeforeEach
    public void init() {
        String accountNumber = "3881049878177208181";
        Integer quantity = 2;
        Long teslaStockId = 1L;

        var buyOrder = OrderDto.builder()
                .orderId(1L)
                .orderType(OrderType.BUY)
                .accountNumber(accountNumber)
                .quantity(quantity)
                .targetPrice(new BigDecimal("1720"))
                .stockId(teslaStockId)
                .build();

        var sellOrder = OrderDto.builder()
                .orderId(1L)
                .orderType(OrderType.SELL)
                .accountNumber(accountNumber)
                .quantity(quantity)
                .targetPrice(new BigDecimal("1640"))
                .stockId(teslaStockId)
                .build();

        orderDtoList = List.of(buyOrder, sellOrder);


        var teslaStock = Stock.builder()
                .name("TESLA")
                .id(teslaStockId)
                .price(new BigDecimal("850"))
                .build();

        var bitcoinStock = Stock.builder()
                .name("BITCOIN")
                .id(2L)
                .price(new BigDecimal("890"))
                .build();

        stockList = List.of(teslaStock, bitcoinStock);
    }

    @Test
    @DisplayName("Given nothing when scheduler trigger to complete orders then complete orders")
    void completeOrder() {

        when(orderRepository.getOrders()).thenReturn(orderDtoList);
        when(stockService.getStocks()).thenReturn(stockList);

        doNothing().when(orderRepository).deleteOrder(any());
        doNothing().when(completeOrderMessageSender).send(any());
        doNothing().when(accountCompleteStockOrderMessageSender).send(any());

        orderService.completeOrder();

        verify(orderRepository, times(1)).getOrders();
        verify(stockService, times(1)).getStocks();

    }
}