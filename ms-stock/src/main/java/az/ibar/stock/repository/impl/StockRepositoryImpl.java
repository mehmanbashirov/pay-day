package az.ibar.stock.repository.impl;

import az.ibar.stock.model.Stock;
import az.ibar.stock.repository.StockRepository;
import lombok.RequiredArgsConstructor;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Repository;

import java.util.List;

import static az.ibar.stock.util.DefaultValueUtil.STOCK_BUCKET_NAME;

@Repository
@RequiredArgsConstructor
public class StockRepositoryImpl implements StockRepository {

    private final RedissonClient redissonClient;

    @Override
    public void saveStock(List<Stock> stockList) {
        redissonClient.getList(STOCK_BUCKET_NAME).clear();
        redissonClient.getList(STOCK_BUCKET_NAME).addAll(stockList);
    }

    @Override
    public List<Stock> getStocks() {
        return redissonClient.getList(STOCK_BUCKET_NAME);
    }
}
