package az.ibar.stock.repository;

import az.ibar.stock.model.Stock;

import java.util.List;

public interface StockRepository {

    void saveStock(List<Stock> stockList);

    List<Stock> getStocks();

}
